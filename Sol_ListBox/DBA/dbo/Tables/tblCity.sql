﻿CREATE TABLE [dbo].[tblCity] (
    [CityId]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [CityName] VARCHAR (50) NULL,
    CONSTRAINT [PK_tblCity] PRIMARY KEY CLUSTERED ([CityId] ASC)
);

