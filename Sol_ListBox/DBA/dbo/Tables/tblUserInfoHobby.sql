﻿CREATE TABLE [dbo].[tblUserInfoHobby] (
    [HobbyId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [HobbyName]  VARCHAR (50) NULL,
    [Interested] BIT          NULL,
    [UserId]     NUMERIC (18) NULL,
    CONSTRAINT [PK_tblUserInfoHobby] PRIMARY KEY CLUSTERED ([HobbyId] ASC)
);

