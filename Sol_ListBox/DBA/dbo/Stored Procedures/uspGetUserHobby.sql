﻿CREATE PROCEDURE [dbo].[uspGetUserHobby]
	@Command Varchar(100)=NULL,

	@UserId Numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
AS
	BEGIN

	DECLARE @ErrorMessage Varchar(MAX)

	IF @Command='SelectByIdUserData'
		BEGIN
			
			BEGIN TRANSACTION

				BEGIN TRY

							SELECT 
							U.UserId,
							U.FirstName,
							U.LastName,
							U.Gender,
							U.BirthDate,
							U.Age,
							U.CityId
							FROM tblUserInfo AS U
								WHERE U.UserId=@UserId

						SET @Status=1
						SET @Message='Get Search Data'

					COMMIT TRANSACTION
				END TRY

				BEGIN CATCH
						
					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
						
					SET @Status=0
					SET @Message='Search Exception'

					RAISERROR(@ErrorMessage,16,1)
				END CATCH
	END


	ELSE IF @Command='UserHobbyData'
			BEGIN

			BEGIN TRANSACTION

				BEGIN TRY

				SELECT 
					UH.HobbyId,
					UH.HobbyName,
					UH.Interested
					FROM tblHobby AS UH
						WHERE UH.UserId=@UserId

						SET @Status=1
						SET @Message='Get User Hobby Search Data'

					COMMIT TRANSACTION
				END TRY

				BEGIN CATCH
						
					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
						
					SET @Status=0
					SET @Message='Search Exception'

					RAISERROR(@ErrorMessage,16,1)
				END CATCH
			END

			
	END