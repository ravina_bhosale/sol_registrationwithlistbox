﻿CREATE PROCEDURE uspUser
(
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,
	@HobbyName Varchar(50)=NULL,
	@Interested bit=NULL
)

as  
begin  
	insert into tblUser values(@FirstName,@LastName)
	declare @UserId int=@@Identity
	insert into tblHobby values(@HobbyName,@Interested,@UserId)
end 