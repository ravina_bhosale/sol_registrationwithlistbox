﻿CREATE PROCEDURE [dbo].[uspGetUserData]
	AS
		SELECT 
			U.UserId,
			U.FirstName,
			U.LastName,
			U.BirthDate,
			U.MobileNo
			FROM tblUserInfo AS U