﻿CREATE PROCEDURE uspSearchUser
    @FirstName  VARCHAR(50)=NULL,
    @LastName   VARCHAR(50)=NULL,
	@DOB DateTime=NULL,
    @MobileNo VARCHAR(10)=NULL
AS
BEGIN

    DECLARE @Query NVARCHAR(MAX) = N'SELECT * FROM tblUserInfo WHERE '
    DECLARE  @ParamDefinition NVARCHAR(MAX) = N'@FirstName VARCHAR(20),
                                                @LastName VARCHAR(20),
												@DOB DateTime,
                                                @MobileNo VARCHAR(10)'

   IF @FirstName IS NOT NULL
		BEGIN
			SET @Query=@Query + 'FirstName='+''''+@FirstName+''''
		END
	ELSE IF @LastName IS NOT NULL
		BEGIN
			SET @Query=@Query + ' LastName='+''''+@LastName+''''
		END

		ELSE IF @DOB IS NOT NULL
		BEGIN
			SET @Query=@Query + 'BirthDate='+''''+Convert(varchar(10),@DOB ,101 )+''''
		END

   ELSE IF @MobileNo IS NOT NULL
       SET @Query=@Query + 'MobileNo='+''''+ @MobileNo+''''

    EXECUTE sp_executesql @Query, @ParamDefinition, @FirstName, @LastName,@DOB, @MobileNo;

END
