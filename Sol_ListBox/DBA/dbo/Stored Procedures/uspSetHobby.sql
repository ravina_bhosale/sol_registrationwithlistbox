﻿CREATE PROCEDURE [dbo].[uspSetHobby]
(	@Command Varchar(MAX)=NULL,

	@HobbyId numeric(18,0)=NULL,
	@HobbyName Varchar(50)=NULL,
	@Interested bit,
	@UserId numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)= NULL OUT
) 
AS
	
	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Insert'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					
					INSERT INTO tblHobby
					(
						HobbyName,
						Interested,
						UserId
					)
					VALUES
					(
						@HobbyName,
						@Interested,
						@UserId
					)

					SET @Status=1
					SET @Message='Insert Success' 

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

			END 

			ELSE IF @Command='Update'
			BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

							UPDATE tblHobby
								SET 
									HobbyName=@HobbyName,
									Interested=@Interested,
									UserId=@UserId
										where HobbyId=@HobbyId

								SET @Status=1
								SET @Message='Update Succesfully'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH


			END

	END