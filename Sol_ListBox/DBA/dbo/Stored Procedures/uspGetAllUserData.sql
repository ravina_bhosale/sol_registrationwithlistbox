﻿CREATE PROCEDURE [dbo].[uspGetAllUserData]
	
	@Command varchar(50)=NULL,

	@UserId numeric(18,0)=NULL,

	@Status int=NULL out,
	@Message varchar(max)=NULL out

AS
	BEGIN

	DECLARE @ERRORMESSAGE varchar(max)=NULL

	IF @Command='SelectUserData'
		
		BEGIN
			BEGIN TRANSACTION

			BEGIN TRY
				SELECT
				 U.UserId,
				 U.FirstName,
				 U.LastName,
				 U.Gender,
				 U.BirthDate,
				 U.Age,
				 U.CityId,
				 U.ImagePath	
					FROM tblUserInfo AS U
						INNER JOIN
							tblCity AS C
								ON 
								  U.CityId=C.CityId
									WHERE U.UserId=@UserId

				SET @Status=1
				SET @Message='Select all Data'
				
				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='Stored Proc execution Fail'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

		END

		ELSE IF @Command='SelectHobbyData'
			
			BEGIN
				BEGIN TRANSACTION

				BEGIN TRY
					SELECT 
					 UH.HobbyId,
					 UH.HobbyName,
					 UH.Interested
						FROM tblUserInfoHobby AS UH
							WHERE UH.UserId=@UserId

						SET @Status=1
						SET @Message='SelectAllHobbiesData'

					COMMIT TRANSACTION
				END TRY

				BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='Stored Proc execution Fail'

				RAISERROR(@ErrorMessage,16,1)

				END CATCH
			END

	END