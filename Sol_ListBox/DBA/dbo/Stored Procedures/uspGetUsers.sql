﻿CREATE PROCEDURE [dbo].[uspGetUsers]
	@Command Varchar(100)=NULL,

	@UserId Numeric(18,0)


AS
	
		IF @Command='UserData'
			BEGIN
				SELECT 
					U.UserId,
					U.FirstName,
					U.LastName
					FROM tblUser AS U
						WHERE U.UserId=@UserId
			END
		ELSE IF @Command='UserHobbyData'
			BEGIN
				SELECT 
					UH.HobbyId,
					UH.HobbyName,
					UH.Interested
					FROM tblHobby AS UH
						WHERE UH.UserId=@UserId
			END

