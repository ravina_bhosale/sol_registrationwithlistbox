﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ListBox.Models
{
    public class CityEntity
    {
        public decimal? CityId { get; set; }

        public string CityName { get; set; }
    }
}