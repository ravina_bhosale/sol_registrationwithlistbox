﻿using Sol_ListBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ListBox.Abstract
{
    public abstract class UserAbstract : IDisposable
    {
        public abstract void Dispose();

        public abstract Task<IEnumerable<CityEntity>> GetCityDataAsync();

        public abstract Task<Boolean> SetUserData(UserEntity userEntityObj);

        public abstract Task<UserEntity> SearchUserDataAsync(UserEntity userEntityObj);

        public abstract Task<IEnumerable<UserEntity>> GetAllUserData();

        public abstract Task<dynamic> SerachUser(UserEntity userEntityobj);
    }
}