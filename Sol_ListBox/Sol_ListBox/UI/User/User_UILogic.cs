﻿using Sol_ListBox.BAL;
using Sol_ListBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_ListBox.UI.User
{
    public partial class UserInfo
    {
        #region Declaration
        private UserContext userContextObj = null;
        #endregion

        #region Private Method
        private async Task BindCity()
        {
            // Create an Instance of User Bal Object
            userContextObj = new UserContext();

            // Bind City Data
            CityBinding = await userContextObj?.GetCityDataAsync();

            ddlCity.AppendDataBoundItems = true;
            ddlCity.Items.Insert(0, new ListItem("", "0"));
            ddlCity.SelectedIndex = 0;
        }

        /// <summary>
        /// Mapping from Entity to Ui
        /// </summary>
        /// <returns></returns>
        private async Task DataMapping(UserEntity userEntityObj)
        {
            await Task.Run(() => {

                FirstNameBinding = userEntityObj.FirstName;
                LastNameBinding = userEntityObj.LastName;
                DobBinding = userEntityObj.BirthDate;
                AgeBinding = Convert.ToInt32(userEntityObj.Age);
                GenderBinding = Convert.ToBoolean(userEntityObj.Gender);
                CityBinding = Convert.ToString(userEntityObj.CityId);
                UserProfilePicBinding = userEntityObj.ImagePath;
                MobileNoBinding = userEntityObj.MobileNo;
                HobbyBinding = userEntityObj.UserHobby;
            });
        }

        /// <summary>
        /// Mapping from Ui to Entity
        /// </summary>
        /// <returns></returns>
        private async Task<UserEntity> DataMapping()
        {
            return await Task.Run(() =>
            {

                return new UserEntity()
                {
                    FirstName = FirstNameBinding,
                    LastName = LastNameBinding,
                    BirthDate = DobBinding,
                    Age = Convert.ToInt32(AgeBinding),
                    Gender = GenderBinding,
                    UserHobby = HobbyBinding,
                    CityId = CityBinding,
                    ImagePath = UserProfilePicBinding
                };

            });
        }

        private async Task BindUserData()
        {
            // Create an Instance of Product Dal
            userContextObj = new UserContext();

            //get UserData
            var userObj = await userContextObj?.SearchUserDataAsync(new UserEntity()
            {
                UserId = Convert.ToDecimal(lbUser.SelectedValue)
                
            });
            await this.DataMapping(userObj);
        }

        private async Task BindUserListBox()
        {
            // Create an Instance of Product Dal
            userContextObj = new UserContext();

            // Get List of Product Entity
            List<UserEntity> listUserEntity = await userContextObj?.GetAllUserData() as List<UserEntity>;

            // Bind List
            this.UserListBinding = listUserEntity;
        }
        private async Task BindSearchUserListBox()
        {
            await Task.Run(async () =>
            {
                // Create an Instance of Product Dal
                userContextObj = new UserContext();

                // Get List of Product Entity
                List<UserEntity> listUserEntityObj = await userContextObj?.SearchUserDataAsync(await SerchTextBoxBind());

                // Bind List
                this.UserListBinding = listUserEntityObj;
            });
           
        }
        private async Task UpdateUserData()
        {
           // Create an instance of User Dal.
            userContextObj = new UserContext();

            // add Data to Database and Bind to the lable
            await userContextObj.SetUserData(await this.DataMapping());

        }

        private async Task SerchTextBoxBind()
        {
             await Task.Run(() =>
            {
                UserEntity userObj = new UserEntity();

                if (ddlSeach.Text == "1")
                {
                    userObj.FirstName = this.SreachTextBoxBinding;
                }
                else if (ddlSeach.Text == "2")
                {
                    userObj.LastName = this.LastNameBinding;
                }
                else if (ddlSeach.Text == "3")
                {
                    userObj.MobileNo = this.MobileNoBinding;
                }

                return userObj;
            });
               
            }


        #endregion

    }
}