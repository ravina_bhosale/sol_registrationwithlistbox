﻿using Sol_ListBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_ListBox.UI.User
{
    public partial class UserInfo
    {
        #region Property_Binding
        private string SreachTextBoxBinding
        {
            get
            {
               return txtSerch.Text;
            }
            set
            {
                txtSerch.Text = value;
            }
        }

        private string MobileNoBinding
        {
            get
            {
                return txtMobileNo.Text;
            }
            set
            {
                txtMobileNo.Text = value;
            }
        }

        private String FirstNameBinding
        {
            get
            {
                return Server.HtmlEncode(txtFirstName.Text.Trim());
            }
            set
            {
                txtFirstName.Text = Server.HtmlDecode(value.Trim());
            }
        }

        private String LastNameBinding
        {
            get
            {
                return Server.HtmlEncode(txtLastName.Text.Trim());
            }
            set
            {
                txtLastName.Text = Server.HtmlDecode(value.Trim());
            }


        }

        private Boolean GenderBinding
        {
            get
            {
                return
                    (rbGender
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Where((leListItemObj) => leListItemObj.Selected == true)
                    ?.FirstOrDefault()
                    ?.Text) == "Male" ? true : false;
            }
            set
            {
                var flag = value;

                rbGender
                   ?.Items
                   ?.Cast<ListItem>()
                   ?.AsEnumerable()
                   ?.ToList()
                   ?.ForEach((leListItemObj) => {

                       if (flag == false)
                       {
                           if (leListItemObj.Text == "Female")
                           {
                               leListItemObj.Selected = true;
                           }
                       }
                       else
                       {
                           
                           if (leListItemObj.Text == "Male")
                           {
                               leListItemObj.Selected = true;
                           }
                       }
                   });
            }
        }


        private String DobBinding
        {
            get
            {
                return
                    txtBirthDate.Text;
            }
            set
            {
                txtBirthDate.Text = value;
            }
        }

        private int AgeBinding
        {
            get
            {
                return
                    Convert.ToInt32(lblAge.Text);
            }
            set
            {
                lblAge.Text = value.ToString();
            }
        }

      

        private List<HobbyEntity> HobbyBinding
        {
            get
            {

                return chkHobbies
                    .Items
                    .Cast<ListItem>()
                    .AsEnumerable()
                    .Select((leCheckBoxListObj) => new HobbyEntity()
                    {
                        HobbyName = leCheckBoxListObj.Text,
                        Interested = leCheckBoxListObj.Selected
                    })
                   .ToList();

            }
            set
            {
                chkHobbies.DataSource = value;
                chkHobbies.DataBind();

                // Selected Property Bind for selected Interested Checkbox from Entity to ui
                foreach (ListItem lstObj in chkHobbies.Items)
                {
                    lstObj.Selected = Convert.ToBoolean(
                            value
                            ?.AsEnumerable()
                            ?.Where((leUserentityObj) => leUserentityObj.HobbyName == lstObj.Text)
                            ?.FirstOrDefault()
                            ?.Interested);
                }
            }
        }

        private dynamic CityBinding
        {
            get
            {
                return
                    ddlCity.SelectedValue;
            }
            set
            {
                if (value is List<CityEntity>)
                {
                    List<CityEntity> listCityEntityObj = value as List<CityEntity>;
                    ddlCity.DataSource = listCityEntityObj;
                    ddlCity.DataBind();
                }
                else if (value is String)
                {
                    ddlCity.SelectedValue = value;
                }

               
            }
        }


        private String UserProfilePicBinding
        {
            get
            {
                return
                       imgUserProfilePic.ImageUrl;
            }
            set
            {
                imgUserProfilePic.ImageUrl = value;
            }
        }

        private List<UserEntity> UserListBinding
        {
            get
            {
                return
                    (List<UserEntity>)lbUser.DataSource;                         
            }
            set
            {
                List<UserEntity> listUserEntityObj = value;

                lbUser.DataSource = listUserEntityObj;
                lbUser.DataBind();
            }
        }



        #endregion
    }
}