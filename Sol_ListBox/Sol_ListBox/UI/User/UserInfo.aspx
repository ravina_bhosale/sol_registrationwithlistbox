﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="Sol_ListBox.UI.User.UserInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        table {
            width: 60%;
            margin: auto;
        }

        .img-Circle {
            border: 2px;
            border-color:black;
            border-style:solid;
            border-radius: 50%;
            height: 150px;
            width: 150px;
        }
    </style>


</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlSeach" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="ddlSeach_SelectedIndexChanged">
                                    <asp:ListItem Text="FirstName" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="LastName" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="BirthDate" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="MobileNo" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                             <td>
                                <asp:TextBox ID="txtSerch" runat="server" ></asp:TextBox>

                                <asp:Button ID="btnSeach" Text="Search" runat="server" OnClick="btnSeach_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lbUser" runat="server"  DataTextField="FullNameMobileNo" DataValueField="UserId" AutoPostBack="true" OnSelectedIndexChanged="lbUser_SelectedIndexChanged" Height="200px" Width="200px"></asp:ListBox>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Image ID="imgUserProfilePic" runat="server" CssClass="img-Circle" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtLastName" runat="server" placeholder="LastName"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtMobileNo" runat="server" placeholder="MobileNo"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblGender" runat="server" Text="Gender :"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbGender" runat="server">
                                                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtBirthDate" runat="server" TextMode="Date"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblAge" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblHobbies" runat="server" Text="Hobbies : "></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBoxList ID="chkHobbies" runat="server" DataTextField="HobbyName" DataValueField="HobbyId">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="CityId"></asp:DropDownList>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
