﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ListBox.UI.User
{
    public partial class UserInfo : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                //await this.BindUserListBox();

                await this.BindCity();
            }

        }

        protected async void lbUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            await this.BindUserData();
        }

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            await this.UpdateUserData();
        }

        protected async void btnSeach_Click(object sender, EventArgs e)
        {
            await this.BindSearchUserListBox();
        }

        protected void ddlSeach_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }
    }
}