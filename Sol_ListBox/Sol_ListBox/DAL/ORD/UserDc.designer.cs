﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sol_ListBox.DAL.ORD
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="UserInfo")]
	public partial class UserDcDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public UserDcDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["UserInfoConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public UserDcDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserDcDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserDcDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserDcDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<uspGetCityResultSet> uspGetCityResultSets
		{
			get
			{
				return this.GetTable<uspGetCityResultSet>();
			}
		}
		
		public System.Data.Linq.Table<uspGetUserDataResultSet> uspGetUserDataResultSets
		{
			get
			{
				return this.GetTable<uspGetUserDataResultSet>();
			}
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspGetAllUserData")]
		public ISingleResult<uspGetUserDataResultSet> uspGetAllUserData([global::System.Data.Linq.Mapping.ParameterAttribute(Name="Command", DbType="VarChar(50)")] string command, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UserId", DbType="Decimal(18,0)")] System.Nullable<decimal> userId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Status", DbType="Int")] ref System.Nullable<int> status, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Message", DbType="VarChar(MAX)")] ref string message)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), command, userId, status, message);
			status = ((System.Nullable<int>)(result.GetParameterValue(2)));
			message = ((string)(result.GetParameterValue(3)));
			return ((ISingleResult<uspGetUserDataResultSet>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspGetCity")]
		public ISingleResult<uspGetCityResultSet> uspGetCity()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<uspGetCityResultSet>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspSetUserInfo")]
		public int uspSetUserInfo([global::System.Data.Linq.Mapping.ParameterAttribute(Name="Command", DbType="VarChar(MAX)")] string command, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UserId", DbType="Decimal(18,0)")] System.Nullable<decimal> userId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="FirstName", DbType="VarChar(50)")] string firstName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="LastName", DbType="VarChar(50)")] string lastName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Gender", DbType="Bit")] System.Nullable<bool> gender, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="CityId", DbType="Decimal(18,0)")] System.Nullable<decimal> cityId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="BirthDate", DbType="DateTime")] System.Nullable<System.DateTime> birthDate, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Age", DbType="Int")] System.Nullable<int> age, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="ImagePath", DbType="NVarChar(MAX)")] string imagePath, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Status", DbType="Int")] ref System.Nullable<int> status, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Message", DbType="VarChar(MAX)")] ref string message, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Id", DbType="Decimal(18,0)")] ref System.Nullable<decimal> id)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), command, userId, firstName, lastName, gender, cityId, birthDate, age, imagePath, status, message, id);
			status = ((System.Nullable<int>)(result.GetParameterValue(9)));
			message = ((string)(result.GetParameterValue(10)));
			id = ((System.Nullable<decimal>)(result.GetParameterValue(11)));
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspSetUserInfoHobby")]
		public int uspSetUserInfoHobby([global::System.Data.Linq.Mapping.ParameterAttribute(Name="Command", DbType="VarChar(MAX)")] string command, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="HobbyId", DbType="Decimal(18,0)")] System.Nullable<decimal> hobbyId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="HobbyName", DbType="VarChar(50)")] string hobbyName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Interested", DbType="Bit")] System.Nullable<bool> interested, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UserId", DbType="Decimal(18,0)")] System.Nullable<decimal> userId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Status", DbType="Int")] ref System.Nullable<int> status, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Message", DbType="VarChar(MAX)")] ref string message)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), command, hobbyId, hobbyName, interested, userId, status, message);
			status = ((System.Nullable<int>)(result.GetParameterValue(5)));
			message = ((string)(result.GetParameterValue(6)));
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspGetUserData")]
		public ISingleResult<uspGetUserDataResultSet> uspGetUserData()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<uspGetUserDataResultSet>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspSearchUser")]
		public int uspSearchUser([global::System.Data.Linq.Mapping.ParameterAttribute(Name="FirstName", DbType="VarChar(50)")] string firstName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="LastName", DbType="VarChar(50)")] string lastName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="DOB", DbType="DateTime")] System.Nullable<System.DateTime> dOB, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="MobileNo", DbType="VarChar(10)")] string mobileNo)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), firstName, lastName, dOB, mobileNo);
			return ((int)(result.ReturnValue));
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class uspGetCityResultSet
	{
		
		private System.Nullable<decimal> _CityId;
		
		private string _CityName;
		
		public uspGetCityResultSet()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CityId")]
		public System.Nullable<decimal> CityId
		{
			get
			{
				return this._CityId;
			}
			set
			{
				if ((this._CityId != value))
				{
					this._CityId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CityName")]
		public string CityName
		{
			get
			{
				return this._CityName;
			}
			set
			{
				if ((this._CityName != value))
				{
					this._CityName = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class uspGetUserDataResultSet
	{
		
		private System.Nullable<decimal> _UserId;
		
		private string _FirstName;
		
		private string _LastName;
		
		private System.Nullable<bool> _Gender;
		
		private System.Nullable<System.DateTime> _BirthDate;
		
		private System.Nullable<int> _Age;
		
		private System.Nullable<decimal> _CityId;
		
		private string _ImagePath;
		
		private string _CityName;
		
		private System.Nullable<decimal> _HobbyId;
		
		private string _HobbyName;
		
		private System.Nullable<bool> _Interested;
		
		private string _MobileNo;
		
		public uspGetUserDataResultSet()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UserId")]
		public System.Nullable<decimal> UserId
		{
			get
			{
				return this._UserId;
			}
			set
			{
				if ((this._UserId != value))
				{
					this._UserId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FirstName")]
		public string FirstName
		{
			get
			{
				return this._FirstName;
			}
			set
			{
				if ((this._FirstName != value))
				{
					this._FirstName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastName")]
		public string LastName
		{
			get
			{
				return this._LastName;
			}
			set
			{
				if ((this._LastName != value))
				{
					this._LastName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Gender")]
		public System.Nullable<bool> Gender
		{
			get
			{
				return this._Gender;
			}
			set
			{
				if ((this._Gender != value))
				{
					this._Gender = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_BirthDate")]
		public System.Nullable<System.DateTime> BirthDate
		{
			get
			{
				return this._BirthDate;
			}
			set
			{
				if ((this._BirthDate != value))
				{
					this._BirthDate = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Age")]
		public System.Nullable<int> Age
		{
			get
			{
				return this._Age;
			}
			set
			{
				if ((this._Age != value))
				{
					this._Age = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CityId")]
		public System.Nullable<decimal> CityId
		{
			get
			{
				return this._CityId;
			}
			set
			{
				if ((this._CityId != value))
				{
					this._CityId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ImagePath")]
		public string ImagePath
		{
			get
			{
				return this._ImagePath;
			}
			set
			{
				if ((this._ImagePath != value))
				{
					this._ImagePath = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CityName")]
		public string CityName
		{
			get
			{
				return this._CityName;
			}
			set
			{
				if ((this._CityName != value))
				{
					this._CityName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_HobbyId")]
		public System.Nullable<decimal> HobbyId
		{
			get
			{
				return this._HobbyId;
			}
			set
			{
				if ((this._HobbyId != value))
				{
					this._HobbyId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_HobbyName")]
		public string HobbyName
		{
			get
			{
				return this._HobbyName;
			}
			set
			{
				if ((this._HobbyName != value))
				{
					this._HobbyName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Interested")]
		public System.Nullable<bool> Interested
		{
			get
			{
				return this._Interested;
			}
			set
			{
				if ((this._Interested != value))
				{
					this._Interested = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MobileNo")]
		public string MobileNo
		{
			get
			{
				return this._MobileNo;
			}
			set
			{
				if ((this._MobileNo != value))
				{
					this._MobileNo = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
