﻿using Sol_ListBox.Abstract;
using Sol_ListBox.DAL.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sol_ListBox.Models;
using System.Threading.Tasks;
using System.Text;

namespace Sol_ListBox.DAL
{
    public class UserDal : UserAbstract
    {
        #region Declaration
        private Lazy<UserDcDataContext> dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            dc = new Lazy<UserDcDataContext>(() => new UserDcDataContext());
        }


        #endregion

        #region Public Method
        public override void Dispose()
        {
            dc?.Value.Dispose();
            dc = null;
            GC.SuppressFinalize(this);
        }

        public async override Task<IEnumerable<CityEntity>> GetCityDataAsync()
        {
            try
            {
                return await Task.Run(() =>
                {
                    return dc
                     ?.Value
                     ?.uspGetCity()
                     ?.AsEnumerable()
                     ?.Select((leGetCityData) => new CityEntity()
                     {
                         CityId = leGetCityData?.CityId,
                         CityName = leGetCityData?.CityName
                     }).ToList();
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async override Task<UserEntity> SearchUserDataAsync(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                return await Task.Run(() =>
                {
                    return
                    dc
                    ?.Value
                    ?.uspGetAllUserData
                    (
                        "SelectUserData",
                        userEntityObj?.UserId,
                        ref status,
                        ref message
                     )
                     ?.AsEnumerable()
                     ?.Select((leGetUserData) => new UserEntity()
                     {
                         UserId = leGetUserData?.UserId,
                         FirstName = leGetUserData?.FirstName,
                         LastName = leGetUserData?.LastName,
                         MobileNo=leGetUserData?.MobileNo,
                         FullNameMobileNo = new StringBuilder().
                                            Append(leGetUserData?.FirstName)
                                            .Append("").Append(leGetUserData?.LastName)
                                            .Append("|").Append(leGetUserData?.MobileNo).ToString(),
                         Gender = leGetUserData?.Gender,
                         BirthDate =leGetUserData?.BirthDate.Value.ToString("yyyy-MM-dd"),
                         Age = leGetUserData?.Age,
                         CityId = leGetUserData?.CityId,
                         ImagePath = leGetUserData?.ImagePath,
                         UserHobby = this.GetUserHobbyDataAsync(new HobbyEntity()
                         {
                             UserId = userEntityObj?.UserId,
                         }).Result as List<HobbyEntity>
                     })
                     ?.ToList()
                     ?.FirstOrDefault();
                });
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async override Task<bool> SetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            decimal? id = null;
            try
            {
                return await Task.Run(async () =>
                {
                    var setQuery =
                    dc
                    ?.Value
                    ?.uspSetUserInfo
                    (
                        "Update",
                         userEntityObj?.UserId,
                         userEntityObj?.FirstName,
                         userEntityObj?.LastName,
                         userEntityObj?.Gender,
                         userEntityObj?.CityId,
                        string.IsNullOrEmpty(userEntityObj.BirthDate) ? (DateTime?)null : DateTime.Parse(userEntityObj.BirthDate),
                        userEntityObj?.Age,
                        userEntityObj?.ImagePath,
                        ref status,
                        ref message,
                        ref id
                        );
                    if (id != null)
                    {
                        await this.SetHobbyDataAsync(id, userEntityObj.UserHobby);
                    }

                    return (status == 1) ? true : false;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async override Task<IEnumerable<UserEntity>> GetAllUserData()
        {

            return await Task.Run(() =>
            {

                var getUserDataQuery =
                    dc
                    ?.Value
                    ?.uspGetUserData()
                    ?.AsEnumerable()
                    ?.Select((leUsersObj) => new UserEntity()
                    {
                        UserId = leUsersObj?.UserId,
                        FirstName = leUsersObj?.FirstName,
                        LastName = leUsersObj?.LastName,
                        FullNameMobileNo = new StringBuilder().
                                            Append(leUsersObj?.FirstName)
                                            .Append("").Append(leUsersObj?.LastName)
                                            .Append("|").Append(leUsersObj?.MobileNo).ToString(),
                        BirthDate=leUsersObj?.BirthDate.ToString(),
                        MobileNo=leUsersObj?.MobileNo
                    })
                    ?.ToList();

                return getUserDataQuery;

            });
        }

        public async override Task<dynamic> SerachUser(UserEntity userEntityobj)
        {
            return await Task.Run(() =>
            {

                var getUserDataQuery =
                    dc
                    ?.Value
                    ?.uspSearchUser
                    (
                       userEntityobj?.FirstName,
                       userEntityobj?.LastName,
                       
                       string.IsNullOrEmpty(userEntityobj.BirthDate) ? (DateTime?)null : DateTime.Parse(userEntityobj.BirthDate),
                       userEntityobj?.MobileNo
                     );

                return getUserDataQuery;

            });
        }

        #endregion

        #region PrivateMethod
        private async Task<IEnumerable<HobbyEntity>> GetUserHobbyDataAsync(HobbyEntity hobbyEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                return await Task.Run(() =>
                {
                    return
                    dc
                    ?.Value
                    ?.uspGetAllUserData
                    (
                        "SelectHobbyData",
                        hobbyEntityObj?.UserId,
                        ref status,
                        ref message
                     )
                     ?.AsEnumerable()
                     ?.Select((leGetHobbyData) => new HobbyEntity()
                     {
                         HobbyId = leGetHobbyData?.HobbyId,
                         HobbyName = leGetHobbyData?.HobbyName,
                         Interested = leGetHobbyData?.Interested
                     })
                     ?.ToList();
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<Boolean> SetHobbyDataAsync(decimal? UserId, List<HobbyEntity> userListHobbyEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {

                    userListHobbyEntityObj
                    ?.AsEnumerable()
                    ?.ToList()
                    ?.ForEach((leUserHobbyEntityObj) =>
                    {

                        var setUserHobbyQuery =
                            dc
                            ?.Value
                            ?.uspSetUserInfoHobby
                            (
                                "Update",
                                leUserHobbyEntityObj.HobbyId,
                                leUserHobbyEntityObj.HobbyName,
                                leUserHobbyEntityObj.Interested,
                                UserId,
                                ref status,
                                ref message
                            );

                    });

                    return (status == 1) ? true : false;
                });
            }
            catch (Exception)
            {
                throw;
            }
            #endregion
        }

       
    }
}