﻿using Sol_ListBox.DAL;
using Sol_ListBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ListBox.BAL
{
    public class UserContext : UserDal
    {

        #region Constructor
        public UserContext() : base()
        {

        }
        #endregion

        #region Public Methods
        public async override Task<IEnumerable<CityEntity>> GetCityDataAsync()
        {
            try
            {
                return await base.GetCityDataAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async override Task<UserEntity> SearchUserDataAsync(UserEntity userEntityObj)
        {
            try
            {
                return await base.SearchUserDataAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async override Task<bool> SetUserData(UserEntity userEntityObj)
        {
            try
            {
                return await base.SetUserData(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async override Task<dynamic> SerachUser(UserEntity userEntityobj)
        {
            try
            {
                return await base.SerachUser(userEntityobj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async override Task<IEnumerable<UserEntity>> GetAllUserData()
        {
            try
            {
                return await base.GetAllUserData();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}